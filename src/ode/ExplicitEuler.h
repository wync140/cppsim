#pragma once
#include <chrono>
#include <vector>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include "OdeInterface.h"
#include <functional>

namespace cppsim{
namespace ode{

// TODO Template Microseconds
std::vector<double> explicit_euler(
    std::vector<double> *df(std::vector<double> *, std::chrono::microseconds *),
    const std::vector<double> &y, const std::chrono::microseconds &dt,
    const std::chrono::microseconds &t);

boost::numeric::ublas::vector<double> explicit_euler_us (boost::numeric::ublas::vector<double> (*df)(const boost::numeric::ublas::vector<double> & , const double & ),
    const boost::numeric::ublas::vector<double> &y, const std::chrono::microseconds &dt,
    const std::chrono::microseconds &t);

boost::numeric::ublas::vector<double> explicit_euler(boost::numeric::ublas::vector<double> (*df)(const boost::numeric::ublas::vector<double> & , const double & ),
    const boost::numeric::ublas::vector<double> &y, const double &dt,
    const double &t);

boost::numeric::ublas::vector<double> explicit_euler( std::function<boost::numeric::ublas::vector<double>(const boost::numeric::ublas::vector<double> & , const double & )> df,
    const boost::numeric::ublas::vector<double> &y, const double &dt,
    const double &t);

}
} // namespace cppsim::ode
