#include <chrono>
#include <vector>
#include <functional>
#include <algorithm>
#include "ode/ExplicitEuler.h"
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector.hpp>

using namespace std::chrono;
using namespace boost::numeric;
using std::vector;
using std::transform;
using secs = std::chrono::duration<double, std::ratio<1,1>>;

namespace cppsim{
namespace ode{

// TODO Template Microseconds
vector<double> explicit_euler_us(
    vector<double> (*df)(const vector<double> & , const double & ),
    const vector<double> &y, const microseconds &dt,
    const microseconds &t){

    double t_seconds = secs(t).count();
    double dt_seconds = secs(dt).count();
    
    vector<double> df_dt = df(y,t_seconds) ;

    if (df_dt.size() != y.size() ){
        std::cerr << "vector lengths mismatched in explicit_euler function" << std::endl;
        throw 1;
    }

    
    //transform(df_dt.begin(),df_dt.end(),df_dt.begin(), [&dt_seconds](auto& c){return c*dt_seconds});
    
    for (int i=0; i != df_dt.size(); ++i){
        df_dt[i] = df_dt[i] * dt_seconds + y[i];
    }

    return  df_dt;
    }
    
ublas::vector<double> explicit_euler_us (ublas::vector<double> (*df)(const ublas::vector<double> & , const double & ),
    const ublas::vector<double> &y, const microseconds &dt,
    const microseconds &t){
    double t_seconds = secs(t).count();
    double dt_seconds = secs(dt).count();
    ublas::vector<double> df_dt = df(y,t_seconds) ;
    return (dt_seconds * df_dt) + y;
    }

ublas::vector<double> explicit_euler (ublas::vector<double> (*df)(const ublas::vector<double> & , const double & ),
    const ublas::vector<double> &y, const double &dt,
    const double &t){
    return (dt * df(y,t)) + y;
    }

boost::numeric::ublas::vector<double> explicit_euler(std::function<boost::numeric::ublas::vector<double>(const boost::numeric::ublas::vector<double> & , const double & )> df,
    const boost::numeric::ublas::vector<double> &y, const double &dt,
    const double &t){
    return (dt * df(y,t)) + y;
    }

}
} 
