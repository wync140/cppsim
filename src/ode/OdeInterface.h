#pragma once
#include "boost/numeric/ublas/vector.hpp"
#include <functional>

namespace cppsim {
namespace ode {
  
#define BIND_ODE_FUNCTION(CLASS_NAME, FUNCTION_NAME) (std::bind(&CLASS_NAME::FUNCTION_NAME, &*this,std::placeholders::_1,std::placeholders::_2))

class OdeInterface {
public:
  virtual boost::numeric::ublas::vector<double>
  dy_dt(const boost::numeric::ublas::vector<double> &y, const double &t) = 0;

protected:
  boost::numeric::ublas::vector<double>
  explicit_euler(const boost::numeric::ublas::vector<double> &y,
                 const double &dt, const double &t);
};


} // namespace ode
} // namespace cppsim