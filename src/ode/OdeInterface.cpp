#include "ode/OdeInterface.h"
#include "boost/numeric/ublas/vector.hpp"

namespace cppsim {
namespace ode {
boost::numeric::ublas::vector<double>
OdeInterface::explicit_euler(const boost::numeric::ublas::vector<double> &y,
                             const double &dt, const double &t) {
  return (dt * dy_dt(y, t)) + y;
}
} // namespace ode
} // namespace cppsim