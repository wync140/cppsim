#pragma once
#include "boost/numeric/ublas/vector.hpp"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <cmath>

namespace cppsim {
namespace linalg {

    boost::numeric::ublas::matrix<double> Rx(double theta);
    boost::numeric::ublas::matrix<double> Ry(double theta);
    boost::numeric::ublas::matrix<double> Rz(double theta);
}
} // namespace cppsim