#include "boost/numeric/ublas/vector.hpp"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "linalg/rotation.h"
#include <cmath>

#define PI 3.14159265


namespace cppsim {
namespace linalg {

    boost::numeric::ublas::matrix<double> Rx(double theta){
        const int ROWS = 3;
        const int COLS = 3;
        const double MATRIX_INIT[][3] = {{1.0, 0.0, 0.0}, {0.0, cos(theta) , - sin(theta) }, {0.0, sin(theta), cos(theta) }};
        boost::numeric::ublas::matrix<double> M (ROWS, COLS);
        for (int i = 0; i < ROWS; ++i)
            for (int j = 0; j < COLS; ++j)
                M(i, j) = MATRIX_INIT[i][j];
        return M;
    }

    boost::numeric::ublas::matrix<double> Ry(double theta){
        const int ROWS = 3;
        const int COLS = 3;
        const double MATRIX_INIT[][3] = {{cos(theta), 0.0, sin(theta)}, {0.0, 1.0 , 0.0 }, {-sin(theta), 0.0, cos(theta)}};
        boost::numeric::ublas::matrix<double> M (ROWS, COLS);
        for (int i = 0; i < ROWS; ++i)
            for (int j = 0; j < COLS; ++j)
                M(i, j) = MATRIX_INIT[i][j];
        return M;
    }

    boost::numeric::ublas::matrix<double> Rz(double theta){
        const int ROWS = 3;
        const int COLS = 3;
        const double MATRIX_INIT[][3] = {{ cos(theta), - sin(theta), 0.0 }, {sin(theta), cos(theta), 0.0 }, {0.0, 0.0, 1.0}};
        boost::numeric::ublas::matrix<double> M (ROWS, COLS);
        for (int i = 0; i < ROWS; ++i)
            for (int j = 0; j < COLS; ++j)
                M(i, j) = MATRIX_INIT[i][j];
        return M;
    }
    

}
}

