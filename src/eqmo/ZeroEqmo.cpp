#include "boost/numeric/ublas/vector.hpp"
#include "eqmo/ZeroEqmo.h"

using namespace boost::numeric;

namespace cppsim{
namespace simobjects{

     ublas::vector<double> ZeroEqmo::get_Velocity_body_fps()
     {
        ublas::vector<double> velocity_fps(3);
        velocity_fps(1) = 168;
        return velocity_fps;
     }

     ublas::vector<double> ZeroEqmo::get_Velocity_body_kts()
     {
        ublas::vector<double> velocity_kts(3);
        velocity_kts(1) = 100;
        return velocity_kts;
     }

     ublas::vector<double> ZeroEqmo::get_body_rates_degs()
     {
        ublas::vector<double> rates_degs(3);
        rates_degs(2) = 2;
        return rates_degs;
     }

    ublas::vector<double> ZeroEqmo::get_body_rates_rads()
     {
        ublas::vector<double> rates_rads(3);
        rates_rads(2) = .0349;
        return rates_rads;
     }

}
}