#pragma once
#include "simobjects/SimObject.h"
#include "boost/numeric/ublas/vector.hpp"

namespace cppsim{
namespace simobjects{
    class EqmoInterface{
      public:
        virtual boost::numeric::ublas::vector<double> get_Velocity_body_kts() = 0;
        virtual boost::numeric::ublas::vector<double> get_Velocity_body_fps() = 0;
        virtual boost::numeric::ublas::vector<double> get_body_rates_degs() = 0;
        virtual boost::numeric::ublas::vector<double> get_body_rates_rads() = 0;
    };

}
}