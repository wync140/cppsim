#pragma once
#include "boost/numeric/ublas/vector.hpp"
#include "eqmo/EqmoInterface.h"

namespace cppsim{
namespace simobjects{

    class ZeroEqmo: public SimObject, public EqmoInterface{
        public:
            ZeroEqmo(){};
            void tick(double physics_time_step, double sim_time) override{};
            void init() override{};
            boost::numeric::ublas::vector<double> get_Velocity_body_kts() override;
            boost::numeric::ublas::vector<double> get_Velocity_body_fps() override;
            boost::numeric::ublas::vector<double> get_body_rates_degs() override;
            boost::numeric::ublas::vector<double> get_body_rates_rads() override;

    };

}
}