#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <logger/SimLogger.h>

namespace cppsim{
namespace simobjects{

    class SimObject{
    public:
        virtual void tick(double physics_dt, double sim_time) = 0;
        virtual void init() = 0;
    private:
    };
}
}