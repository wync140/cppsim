#include "boost/numeric/ublas/vector.hpp"
#include "simobjects/SimpleSpringObject.h"
#include <string>
#include <iostream>
#include <fstream>
#include <array>
#include <iostream>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "simobjects/SimObject.h"
#include "ode/ExplicitEuler.h"
#include "ode/OdeInterface.h"

using namespace boost::numeric;

namespace cppsim{
namespace simobjects{

    SimpleSpringObject::SimpleSpringObject(): k(1), m(1), c(1), logger("/tmp/simplespring.log") {};

    void SimpleSpringObject::init(){
        logger.log() << "sim_time (s) x x_dt" << std::endl;
        //initialization of vectors
        Am = 2;
        An = 2;
        Bm = 2;
        Bn = 1;
        Um = 2;
        A = ublas::matrix<double>(Am, An);
        B = ublas::matrix<double>(Bm, Bn);
        y = ublas::vector<double>(Um);

        std::array<double,4> Amat_load = { 0, 1, -k/m, -c/m};
        std::copy(Amat_load.begin(), Amat_load.end(), A.data().begin());

        std::array<double,2> Bmat_load = { 0, 1/m };
        std::copy(Bmat_load.begin(), Bmat_load.end(), B.data().begin());

        std::array<double,2> Ymat_load = { 0, 0 };
        std::copy(Ymat_load.begin(), Ymat_load.end(), y.data().begin());

    }

    void SimpleSpringObject::tick(double physics_time_step, double sim_time){
        auto l = BIND_ODE_FUNCTION(SimpleSpringObject,dy_dt);
        y = ode::explicit_euler(l, y, physics_time_step, sim_time);

        // log out to file location
        logger.log() << sim_time  << " " << y(0) << " " << y(1) << std::endl;
    }

    ublas::vector<double> SimpleSpringObject::dy_dt(const ublas::vector<double> &y, const double &t){
        return prod(A,y) + prod(B,u(t));
    }

    ublas::vector<double> SimpleSpringObject::u(double t){
        ublas::vector<double> u = ublas::vector<double>(1);
        if (t>1.0){
            u(0) = 5;
        } else {
            u(0) = 0;
        }
        return u;
    }


} 
}