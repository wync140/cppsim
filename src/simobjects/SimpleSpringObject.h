#pragma once
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <string>
#include "simobjects/SimObject.h"
#include "ode/OdeInterface.h"

namespace cppsim{
namespace simobjects{

    // TODO: add printing to specific log and explicit euler abstract class based on SimCollected
    class SimpleSpringObject: public cppsim::simobjects::SimObject{
        public:
            SimpleSpringObject();

            void tick(double physics_time_step, double sim_time) override;
            void init() override;

        private:
            boost::numeric::ublas::vector<double> dy_dt(const boost::numeric::ublas::vector<double> &y, const double &t );
            boost::numeric::ublas::vector<double> u(double t);
            double k;
            double c;
            double m;
            boost::numeric::ublas::matrix<double>::size_type Am;
            boost::numeric::ublas::matrix<double>::size_type An;
            boost::numeric::ublas::matrix<double>::size_type Bm;
            boost::numeric::ublas::matrix<double>::size_type Bn;
            boost::numeric::ublas::vector<double>::size_type Um;
            boost::numeric::ublas::matrix<double> A;
            boost::numeric::ublas::matrix<double> B;
            boost::numeric::ublas::vector<double> y;        
            logger::SimLogger logger;
    };

}
}