#pragma once
#include <string>
#include <iostream>
#include <fstream>

namespace cppsim{
namespace logger{

    class SimLogger{
        public:
            SimLogger(std::string name);
            std::ofstream log();
            void set_log_file_location(std::string);
            void clear_log_file();
        private:
            std::string log_file_location;
            std::ofstream log_file;
    };

}
}