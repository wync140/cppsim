#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include "logger/SimLogger.h"

using std::string;

namespace cppsim{
namespace logger{

    SimLogger::SimLogger(string name): log_file_location(name) { clear_log_file(); };
    
    void SimLogger::set_log_file_location(string location){
        clear_log_file();
        log_file.close();
        log_file_location = location;
    }

    std::ofstream SimLogger::log(){
        log_file.open(log_file_location, std::ofstream::out | std::ofstream::app );
        return std::move(log_file);
    }

    void SimLogger::clear_log_file(){
        log_file.open(log_file_location);
    }

}
}


// int main() {
//     std::cout << "tf vs code"<<std::endl;
//     cppsim::logger::SimLogger logger("/tmp/firstlog.txt");
//     logger.log() << "first Output" << std::endl;
//     logger.log() << "2nd Output" << std::endl;
//     logger.set_log_file_location("/tmp/cleared.log");
//     logger.log() << "another one" << std::endl;
// }