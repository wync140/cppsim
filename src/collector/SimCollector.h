#pragma once

#include "rtc/realtimeclock.h"
#include "simobjects/SimObject.h"
#include <chrono>
#include <memory>
#include <vector>


namespace cppsim {
namespace collector {

class SimCollector : public cppsim::simobjects::SimObject {
public:
  SimCollector();
  void tick(double physics_dt, double sim_time) override;
  void init() override;
  void register_sim_objects(
      std::vector<std::shared_ptr<simobjects::SimObject>> &&objects);

private:
  std::vector<std::shared_ptr<simobjects::SimObject>> simulation_objects;
};

} // namespace collector
} // namespace cppsim
