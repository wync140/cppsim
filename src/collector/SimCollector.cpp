#include "simobjects/SimpleSpringObject.h"
#include <collector/SimCollector.h>
#include <rtc/realtimeclock.h>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <chrono>
#include <iostream>
#include <memory>

#include "simobjects/SimObject.h"

using namespace std::chrono;
using std::unique_ptr;
using std::shared_ptr;
using secs = std::chrono::duration<double, std::ratio<1, 1>>;

namespace cppsim {
namespace collector {

SimCollector::SimCollector(){};

void SimCollector::init() {
  for (int i = 0; i != simulation_objects.size(); ++i) {
    // step through simulation objects on time.W
    simulation_objects[i]->init();
  }
}

void SimCollector::tick(double physics_dt, double sim_time) {
  for (int i = 0; i != simulation_objects.size(); ++i) {
    // step through simulation objects on time.
    simulation_objects[i]->tick(physics_dt, sim_time);
  }
}

void SimCollector::register_sim_objects(
    std::vector<shared_ptr<simobjects::SimObject>> &&objects) {
  simulation_objects = std::move(objects);
}

} // namespace collector
} // namespace cppsim