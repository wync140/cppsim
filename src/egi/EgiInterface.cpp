#include "boost/numeric/ublas/vector.hpp"
#include "egi/EgiInterface.h"

using namespace boost::numeric;

namespace cppsim {
namespace simobjects {
  //ublas::vector<double> EgiInterface::get_dphi_theta_psi_deg_dt() {
  //  return dphi_theta_psi_deg_dt_;
 // }

  ublas::vector<double> EgiInterface::get_phi_theta_psi_rad() {
    return phi_theta_psi_rad;
  }

  ublas::vector<double> EgiInterface::get_phi_theta_psi_deg() {
    return phi_theta_psi_deg;
  }

  double EgiInterface::get_phi_rad(){
    return phi_rad;
  }

  double EgiInterface::get_theta_rad(){
    return theta_rad;
  }

  double EgiInterface::get_psi_rad(){
    return psi_rad;
  }



} // namespace simobjects
} // namespace cppsim