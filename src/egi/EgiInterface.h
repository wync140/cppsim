#pragma once
#include "boost/numeric/ublas/vector.hpp"

namespace cppsim{
namespace simobjects{
    class EgiInterface{
        // get phi theta psi euler angles
        public:
            //boost::numeric::ublas::vector<double> get_dphi_theta_psi_deg_dt();
            //boost::numeric::ublas::vector<double> get_dphi_theta_psi_rad_dt();
            boost::numeric::ublas::vector<double> get_phi_theta_psi_rad();
            boost::numeric::ublas::vector<double> get_phi_theta_psi_deg();
            double get_phi_rad();
            double get_theta_rad();
            double get_psi_rad();
        protected:
            // egi calculated values (outputs)
            boost::numeric::ublas::vector<double> xyz_inertial_ft_;
            boost::numeric::ublas::vector<double> uvw_inertial_fps_;
            boost::numeric::ublas::vector<double> phi_theta_psi_deg;
            boost::numeric::ublas::vector<double> phi_theta_psi_rad;
            //boost::numeric::ublas::vector<double> dphi_theta_psi_deg_dt_;
            double phi_rad;
            double theta_rad;
            double psi_rad;
    };
}
}