#include "egi/BasicEgi.h"
#include "eqmo/EqmoInterface.h"
#include "linalg/rotation.h"
#include "ode/ExplicitEuler.h"
#include "ode/OdeInterface.h"
#include "simobjects/SimObject.h"
#include <algorithm>
#include <cmath>
#include <thread>

#include <iostream>
#include <fstream>

using namespace boost::numeric;

// maximum theta is allowed to be
#define THETA_MAX 1.560051
#define PI 3.14159265

namespace cppsim { 
namespace simobjects {

BasicEgi::BasicEgi(std::shared_ptr<EqmoInterface> eqmo) : eqmo_(eqmo), logger_("/tmp/basic_egi.log") 
{
  xyz_inertial_ft_  = ublas::vector<double>(3);
  uvw_inertial_fps_ = ublas::vector<double>(3);
  phi_theta_psi_deg = ublas::vector<double>(3);
  phi_theta_psi_rad = ublas::vector<double>(3);
}

void BasicEgi::init(){
  std::ofstream log_file;
  log_file.open("/tmp/basic_egi.log", std::ofstream::out | std::ofstream::app );
  log_file << "sim_time x y z" << std::endl;
  
}

void BasicEgi::tick(double physics_time_step, double sim_time) {

  // update Inputs
  this->physics_time_step = physics_time_step;
  this->sim_time = sim_time;
  update_eqmo_vars();

  // do integration
  ublas::vector<double> phi_theta_psi_rad_update = update_euler_angles();
  ublas::vector<double> xyz_inertial_ft_update = update_position();

  // update member variables
  convert_eulers(phi_theta_psi_rad_update);
  uvw_inertial_fps_ = dx_intertial_dt(xyz_inertial_ft_update,sim_time);
  xyz_inertial_ft_ = xyz_inertial_ft_update;
  logger_.log() << sim_time  << " " << xyz_inertial_ft_(0) << " " << xyz_inertial_ft_(1) << " " << xyz_inertial_ft_(2) << std::endl;
}

void BasicEgi::update_eqmo_vars() {
  PQR_body_degs_ = eqmo_->get_body_rates_degs();
  PQR_body_rads_ = eqmo_->get_body_rates_rads();
  uvw_body_fps_ = eqmo_->get_Velocity_body_fps();
}

ublas::vector<double> BasicEgi::update_euler_angles() {
  auto dy_dt_bind = BIND_ODE_FUNCTION(BasicEgi, dflightpath_dt);
  ublas::vector<double> phi_theta_psi_rad_update(3);
  phi_theta_psi_rad_update = ode::explicit_euler(
      dy_dt_bind, phi_theta_psi_rad, physics_time_step, sim_time);
  phi_theta_psi_rad_update(2) =
      std::clamp(phi_theta_psi_rad_update(2), -THETA_MAX, THETA_MAX);
  return phi_theta_psi_rad_update;
}

ublas::vector<double> BasicEgi::dflightpath_dt(const ublas::vector<double> &y,
                                               const double &t) {

  ublas::vector<double> d_euler_angles_dt(3);

  d_euler_angles_dt(0) =
      PQR_body_rads_(0) +
      PQR_body_rads_(1) * sin(phi_rad) * sin(theta_rad) / cos(theta_rad) +
      PQR_body_rads_(2) * cos(phi_rad) * sin(theta_rad) / cos(theta_rad);

  d_euler_angles_dt(1) =
      PQR_body_rads_(1) * cos(phi_rad) - PQR_body_rads_(2) * sin(phi_rad);

  d_euler_angles_dt(2) =
      (PQR_body_rads_(1) * sin(phi_rad) + PQR_body_rads_(2) * cos(phi_rad)) /
      cos(theta_rad);

  return d_euler_angles_dt;
}

void BasicEgi::convert_eulers(ublas::vector<double> phi_theta_psi_rad_update) {
  phi_theta_psi_rad = phi_theta_psi_rad_update;
  phi_theta_psi_deg = phi_theta_psi_rad * PI / 180;
  phi_rad = phi_theta_psi_rad_update(0);
  theta_rad = phi_theta_psi_rad_update(1);
  psi_rad = phi_theta_psi_rad_update(2);
}

ublas::vector<double> BasicEgi::dx_intertial_dt(const ublas::vector<double> &y,
                                                const double &t) {                                           
  ublas::matrix<double> RxLocal = linalg::Rx(phi_rad);
  ublas::matrix<double> RyLocal = linalg::Ry(theta_rad);
  ublas::matrix<double> RzLocal = linalg::Rz(psi_rad);
  ublas::matrix<double>full_rotation = ublas::prod(RzLocal, ublas::matrix<double>(prod(RyLocal,RxLocal)));
  return ublas::prod(full_rotation,uvw_body_fps_);                                        

}

ublas::vector<double> BasicEgi::update_position() {
  ublas::vector<double> xyz_inertial_ft_update(3);
  auto dy_dt_bind = BIND_ODE_FUNCTION(BasicEgi, dx_intertial_dt);
  xyz_inertial_ft_update = ode::explicit_euler(
      dy_dt_bind, xyz_inertial_ft_, physics_time_step, sim_time);
  return xyz_inertial_ft_update;
}

} // namespace simobjects
} // namespace cppsim