#pragma once
#include "simobjects/SimObject.h"
#include "eqmo/EqmoInterface.h"
#include "egi/EgiInterface.h"
#include "logger/SimLogger.h"

namespace cppsim{
namespace simobjects{

    class BasicEgi: public SimObject, public EgiInterface {
        public:
            BasicEgi(std::shared_ptr<EqmoInterface> eqmo);
            void tick(double physics_time_step, double sim_time) override;
            void init() override;
        private:
            // eqmo grabbed values (inputs)
            std::shared_ptr<EqmoInterface> eqmo_;
            boost::numeric::ublas::vector<double> PQR_body_degs_;
            boost::numeric::ublas::vector<double> PQR_body_rads_;
            boost::numeric::ublas::vector<double> uvw_body_fps_;
            double physics_time_step;
            double sim_time;
            void update_eqmo_vars();

            // limit theta to +-89.95 deg here
            boost::numeric::ublas::vector<double> update_euler_angles();
            boost::numeric::ublas::vector<double> update_position();

            // derivatives to be integrated
            boost::numeric::ublas::vector<double> dflightpath_dt(const boost::numeric::ublas::vector<double> &y, const double &t );
            boost::numeric::ublas::vector<double> dx_intertial_dt(const boost::numeric::ublas::vector<double> &y, const double &t );

            // helpers
            // take new phi theta psi and refresh all member vars
            void convert_eulers(boost::numeric::ublas::vector<double> phi_theta_psi_rad_update);
            logger::SimLogger logger_;
            
            
    };
}    
}