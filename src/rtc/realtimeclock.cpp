// empty source file, defined in realtimeclock.h
#include "rtc/realtimeclock.h"
#include <simobjects/SimObject.h>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <chrono>
#include <csignal>
#include <iostream>
#include <math.h>
#include <memory>
#include <thread>

using namespace std::chrono;
using secs = std::chrono::duration<double, std::ratio<1, 1>>;

namespace cppsim {
namespace rtc{

BaseClock::BaseClock(std::unique_ptr<simobjects::SimObject> ticked_object)
    : frame_count(0), frequency_hz(60), run_flag(0), sim_elapsed_time(0),
      runtime(0), frame_time_left(0), sim_object(std::move(ticked_object)) {
  dt = microseconds((int)std::round(1000000.0 / 60));
};

BaseClock::BaseClock(std::unique_ptr<simobjects::SimObject> ticked_object,
                     double Hz)
    : frame_count(0), run_flag(0), sim_elapsed_time(0), runtime(0), frame_time_left(0),
      sim_object(std::move(ticked_object)) {
  frequency_hz = Hz;
  dt = microseconds((int)std::round(1000000.0 / Hz));
};

void BaseClock::run() {

  // start simulation
  run_flag = 1;
  BOOST_LOG_TRIVIAL(info) << "Starting Simulation ";
  auto start_time_ms = steady_clock::now();

  // init the sim object bound to the clock
  sim_object->init();

  double sim_time_double = 0;
  double physics_time_step_double = secs(dt).count();
  

  while (run_flag == 1) {
    // record time before work
    ++frame_count;
    auto frame_start_time_ms = steady_clock::now();

    // do work
    double sim_time_double = secs(sim_elapsed_time).count();
    sim_object->tick(physics_time_step_double, sim_time_double);

    auto calc_done_time_ms = steady_clock::now();
    microseconds total_calc_time = microseconds(
        duration_cast<microseconds>(calc_done_time_ms - frame_start_time_ms)
            .count());
    frame_time_left = frame_time_left + (dt - total_calc_time);

    if (total_calc_time < dt) {
      std::this_thread::sleep_for(
          std::chrono::microseconds(dt - total_calc_time));
    }

    sim_elapsed_time = sim_elapsed_time + dt;
    sim_elapsed_time_double = secs(sim_elapsed_time).count();

    // BOOST_LOG_TRIVIAL(trace) << sim_elapsed_time.count();
    // BOOST_LOG_TRIVIAL(trace) << sim_elapsed_time_double;
    // kill the sim if elapsed time is over. Only do if runtime is actually set
    // and not 0
    if (runtime != 0 && sim_elapsed_time_double > double(runtime)) {
      stop();
      break;
    }
  }
}

void BaseClock::stop() {
  // stops the simulation
  run_flag = 0;
  BOOST_LOG_TRIVIAL(debug) << "";
  BOOST_LOG_TRIVIAL(debug) << "SIM EXITING DUE TO RUNTIME FLAG BEING SET";
  BOOST_LOG_TRIVIAL(debug) << "Time Requested:          " << runtime;
  BOOST_LOG_TRIVIAL(debug) << "Time Simulated:          "
                           << sim_elapsed_time_double;
  BOOST_LOG_TRIVIAL(debug) << "Rough Frames Expected    "
                           << double(runtime) * frequency_hz;
  BOOST_LOG_TRIVIAL(debug) << "Frame count was:         " << frame_count;
  BOOST_LOG_TRIVIAL(debug) << "Ideal Frame Delta was:   " << dt.count();
  BOOST_LOG_TRIVIAL(debug) << "Average Frame Delta was: "
                           << double(runtime) / double(frame_count);
  BOOST_LOG_TRIVIAL(debug) << "Average Post Calc time:  " << frame_time_left.count() / frame_count;
  BOOST_LOG_TRIVIAL(debug) << "Ideal Fps was:           " << frequency_hz;
  BOOST_LOG_TRIVIAL(debug) << "Average Fps  was:        "
                           << double(frame_count) / double(runtime);
}

void BaseClock::set_run_time(unsigned int run_time) { runtime = run_time; }

}
} // namespace cppsim::rtc