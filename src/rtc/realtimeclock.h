#pragma once
#include <simobjects/SimObject.h>
#include <chrono>
#include <memory>

namespace cppsim {
namespace rtc{

class BaseClock {
public:
  BaseClock(std::unique_ptr<simobjects::SimObject>);
  BaseClock(std::unique_ptr<simobjects::SimObject>, double Hz);
  void stop();
  void set_run_time(unsigned int run_time);
  void run();

protected:

private:
  unsigned int runtime;
  long int frame_count;
  double frequency_hz;
  std::chrono::microseconds dt;
  std::chrono::microseconds sim_elapsed_time;
  std::chrono::microseconds frame_time_left;
  double sim_elapsed_time_double;
  bool run_flag;
  std::unique_ptr<simobjects::SimObject> sim_object;
};

}
} // namespace cppsim::rtc
