#include <boost/log/core/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <iostream>
#include "collector/SimCollector.h"
#include "simobjects/SimObject.h"
#include "simobjects/SimpleSpringObject.h"
#include "egi/BasicEgi.h"
#include "eqmo/ZeroEqmo.h"
#include <vector>
#include <memory>

using namespace cppsim;
using std::unique_ptr;

int main() {

  boost::log::core::get()->set_filter(boost::log::trivial::severity >=
                                      boost::log::trivial::trace);

  // Collect sim objects
  //collector::SimCollector sim;
  std::unique_ptr<collector::SimCollector> sim =  std::make_unique<collector::SimCollector>();
  std::vector<std::shared_ptr<simobjects::SimObject>> objects;
  std::shared_ptr<simobjects::SimObject> spring2 = std::make_shared<simobjects::SimpleSpringObject>() ;
  std::shared_ptr<simobjects::ZeroEqmo> eqmo = std::make_shared<simobjects::ZeroEqmo>() ;
  std::shared_ptr<simobjects::BasicEgi> egi = std::make_shared<simobjects::BasicEgi>(eqmo) ;

  objects.push_back(std::move(spring2));
  objects.push_back(std::move(egi));
  objects.push_back(std::move(eqmo));
  sim->register_sim_objects(std::move(objects));

  // make clock object
  rtc::BaseClock clock(std::move(sim),60);
  // set runtime to n seconds
  clock.set_run_time(3);
  // start the simulation
  clock.run();
  return 0;

};