#! /usr/bin/python3
import sys
import matplotlib.pyplot as plt


if __name__ == "__main__":

    with open(sys.argv[1], "r") as f:
        titles = list(map(str, f.readline().rstrip().split()))
        # takes titles and make empty lists dict with
        plot_vectors = [ [] for i in range(len(titles)) ]
        

        line = 1
        while line:
            line = f.readline()
            input_list = list(map(float, line.rstrip().split()))
            if input_list:
                for i in range(len(titles)):
                    plot_vectors[i].append(input_list[i])
    
    for i in range(1,len(titles)):
        plt.figure()
        # zero is always the common x axis
        plt.plot(plot_vectors[0], plot_vectors[i])
        plt.xlabel(titles[0])
        plt.ylabel(titles[i])
    plt.show()
